Summary:             Enterprise Job Scheduler for Java
Name:                quartz
Version:             2.2.1
Release:             2
Epoch:               0
License:             ASL 2.0
URL:                 http://www.quartz-scheduler.org/
# svn export http://svn.terracotta.org/svn/quartz/tags/quartz-2.2.1
# tar caf quartz-2.2.1.tar.xz quartz-2.2.1
Source0:             quartz-%{version}.tar.xz
Patch6000:           CVE-2019-13990.patch
BuildRequires:       maven-local maven-antrun-plugin maven-checkstyle-plugin maven-dependency-plugin
BuildRequires:       maven-enforcer-plugin maven-release-plugin maven-shade-plugin maven-shared
BuildRequires:       rmic-maven-plugin mvn(com.mchange:c3p0) mvn(javax.mail:mail) >= 1.4.3
BuildRequires:       mvn(javax.xml.bind:jaxb-api) mvn(log4j:log4j:1.2.17)
BuildRequires:       mvn(org.apache.geronimo.specs:specs:pom:)
BuildRequires:       mvn(org.apache.geronimo.specs:geronimo-commonj_1.1_spec)
BuildRequires:       mvn(org.apache.geronimo.specs:geronimo-ejb_2.1_spec)
BuildRequires:       mvn(org.apache.geronimo.specs:geronimo-jms_1.1_spec)
BuildRequires:       mvn(org.apache.geronimo.specs:geronimo-jta_1.1_spec)
BuildRequires:       mvn(org.apache.tomcat:tomcat-servlet-api) mvn(org.slf4j:slf4j-api)
BuildRequires:       mvn(org.slf4j:slf4j-log4j12) mvn(asm:asm) mvn(commons-io:commons-io)
BuildRequires:       mvn(junit:junit) mvn(org.apache.derby:derby)
BuildRequires:       mvn(org.hamcrest:hamcrest-library) >= 1.2
BuildArch:           noarch
%description
Quartz is a job scheduling system that can be integrated with, or used
along side virtually any J2EE or J2SE application. Quartz can be used
to create simple or complex schedules for executing tens, hundreds, or
even tens-of-thousands of jobs; jobs whose tasks are defined as standard
Java components or EJBs.

%package javadoc
Summary:             API docs for quartz
%description javadoc
This package contains the API Documentation for quartz.

%prep
%autosetup -p1
%pom_disable_module quartz-jboss
%pom_disable_module quartz-oracle
%pom_disable_module quartz-weblogic
%pom_disable_module terracotta
%pom_remove_plugin org.codehaus.mojo:findbugs-maven-plugin
%pom_remove_plugin :maven-source-plugin
sed -i -e 's/groupId>c3p0</groupId>com.mchange</' **/pom.xml pom.xml
sed -i -e 's/artifactId>junit-dep</artifactId>junit</' **/pom.xml pom.xml
%pom_remove_dep org.apache.openejb:javaee-api quartz-core
%pom_add_dep org.apache.geronimo.specs:geronimo-jta_1.1_spec::provided quartz-core
%pom_add_dep org.apache.tomcat:tomcat-servlet-api::provided quartz-core
%pom_remove_dep org.apache.openejb:javaee-api quartz-jobs
%pom_add_dep org.apache.geronimo.specs:geronimo-ejb_2.1_spec::provided quartz-jobs
%pom_add_dep org.apache.geronimo.specs:geronimo-jms_1.1_spec::provided quartz-jobs
%pom_remove_dep org.apache.openejb:javaee-api quartz-plugins
%pom_add_dep org.apache.geronimo.specs:geronimo-jta_1.1_spec::provided quartz-plugins
%pom_xpath_remove "pom:build/pom:plugins/pom:plugin[pom:artifactId = 'maven-javadoc-plugin' ]/pom:executions" quartz-jobs
sed -i -e 's/<log4j.version>1.2.16/<log4j.version>1.2.17/' pom.xml
%pom_disable_module quartz
%if 0
%pom_remove_plugin org.terracotta:maven-forge-plugin quartz
%pom_remove_plugin :gmaven-plugin quartz
%pom_xpath_remove "pom:build/pom:plugins/pom:plugin[pom:artifactId = 'maven-javadoc-plugin' ]/pom:executions" quartz
%pom_remove_dep org.quartz-scheduler.internal:quartz-jboss quartz
%pom_remove_dep org.quartz-scheduler.internal:quartz-oracle quartz
%pom_remove_dep org.quartz-scheduler.internal:quartz-terracotta-bootstrap quartz
%pom_remove_dep org.quartz-scheduler.internal:quartz-weblogic quartz
sed -i '/org.jboss/d' quartz/pom.xml
sed -i '/org.terracotta.toolkit/d' quartz/pom.xml
sed -i '/weblogic.jdbc/d' quartz/pom.xml
sed -i '/oracle.sql/d' quartz/pom.xml
%endif
cp -p distribution/src/main/assembly/root/licenses/LICENSE.txt .
sed -i 's/\r//' LICENSE.txt
%mvn_file :quartz-core quartz/quartz-core quartz/quartz quartz
%mvn_alias :quartz-core org.quartz-scheduler:quartz

%build
%mvn_build -f -- -Dproject.build.sourceEncoding=UTF-8

%install
%mvn_install

%files -f .mfiles
%dir %{_javadir}/quartz
%doc README.txt
%license LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Sat Sep 19 2020 zhanghua <zhanghua40@huawei.com> - 2.2.1-2
- Fix CVE-2019-13990

* Fri Aug 21 2020 yaokai <yaokai13@huawei.com> - 2.2.1-1
- package init
